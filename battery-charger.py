#!/usr/bin/env python3

# Requirements:
# apt-get install python3-hidapi python3-hid

# GPIO0 -> mosfet to disable discharge
# GPIO1 -> mosfet to disable charge
# GPIO2 -> ADC to sense battery voltage
# GPIO3 -> DAC to set charge current

import hid, hidapi
import time
import json
import argparse

from os import system
system("rmmod hid_mcp2221")

print(hid.enumerate())
gpio = hidapi.Device(vendor_id=1240, product_id=221, blocking=True)

dac_bits = 0
discharge = 0
charge = 0

def writeRam():
    gpio.write(bytes([
        0x60, # Set SRAM settings (cf. datasheet 3.1.13)
        0x00, # don't care
        0x00, # no changes to clock output
        0x85, # DAC Vrm = 2.048V
        0x80 + dac_bits, # new DAC output value
        0x85, # ADC Vrm = 2.048V
        0x00, # no changes to interrupt detection
        0x80, # yes, reconfigure GPIO
        0x00 + (0x0 if discharge else 0x10), # GPIO0 -> gpio, output direction, value
        0x00 + (0x0 if charge else 0x10), # GPIO1 -> gpio, output direction, value
        0x0A, # GPIO2 -> adc1
        0x03, # GPIO3 -> dac
    ]) + b'\x00' * (64 - 12))

    response = gpio.read(64)
    if response[1] != 0:
        raise "failed to configure chip"

    gpio.write(bytes([
        0x60, # Set SRAM settings (cf. datasheet 3.1.13)
        0x00, # don't care
        0x00, # no changes to clock output
        0x85, # DAC Vrm = 2.048V
        0x80 + dac_bits, # new DAC output value
        0x85, # ADC Vrm = 2.048V
        0x00, # no changes to interrupt detection
        0x00, # no, don't reconfigure GPIO this time
        0x00, # don't care
        0x00, # don't care
        0x00, # don't care
        0x00, # don't care
    ]) + b'\x00' * (64 - 12))

    response = gpio.read(64)
    if response[1] != 0:
        raise "failed to configure chip"

def setCharge(on):
    global charge
    charge = on
    writeRam()

def setDischarge(on):
    global discharge
    discharge = on
    writeRam()

def setDAC(voltage):
    global dac_bits

    dac_bits = round(dac_voltage / 2.048 * 32)
    if dac_bits < 0:
        dac_bits = 0
    if dac_bits > 31:
        dac_bits = 31

    print("dac: %f -> %d" % (dac_voltage, dac_bits))
    writeRam()

def setChargeCurrent(mA):
    global dac_bits

    dac_bits = int((mA - 10) / 20)
    if dac_bits < 0:
        dac_bits = 0
    if dac_bits > 31:
        dac_bits = 31

    print("dac: %f mA -> %d bits" % (mA, dac_bits))
    writeRam()

def readADC():
    gpio.write(bytes([
        0x10, # Status
    ]) + b'\x00' * (64 - 1))

    response = gpio.read(64)
    if response[1] != 0:
        raise "failed to query chip"

    voltage_bits = response[52] + response[53] * 256
    voltage = voltage_bits * 2.048 / 1024

    return voltage

def readChipSettings():
    gpio.write(bytes([
        0xb0, # Read Flash Data
        0x00, # Read Chip Settings  (cf. datasheet table 3-5)
    ]) + b'\x00' * (64 - 2))

    response = gpio.read(64)
    if response[1] != 0:
        raise "failed to read current chip settings"

    for i in range(0, 64):
        print("chipSettings[%d] = %x\n" % (i, response[i]))

def writeBootupFlash():
    gpio.write(bytes([
        0xb1, # Write Flash Data
        0x00, # Write Chip Settings  (cf. datasheet table 3-12)
        0x00, # no serial number, no chip locking
        0x02, # unchanged clock divider
        0x80, # dac reference voltage is 2.048V, power-up DAC is 0V
        0x14, # adc reference voltage is 2.048V
        0xd8, # vid low-byte, unchanged at 0xd8
        0x04, # vid high-byte, unchanged at 0x04
        0xdd, # pid low-byte, unchanged at 0xdd
        0x00, # pid high-byte, unchanged at 0x00
        0x80, # usb power attribute, prior default 0x80 aka bus powered
        0xfa, # device will draw <=500mA, prior default 0x32 aka 64mA
    ]) + b'\x00' * (64 - 12))

    response = gpio.read(64)
    if response[1] != 0:
        raise "failed to update bootup flash"

    gpio.write(bytes([
        0xb1, # Write Flash Data
        0x01, # Write GP Settings  (cf. datasheet table 3-13)
        0x10, # bootup GPIO0 -> output, high
        0x10, # bootup GPIO1 -> output, high
        0x00, # bootup GPIO2 -> output, low
        0x08, # bootup GPIO3 -> input
    ]) + b'\x00' * (64 - 6))

    response = gpio.read(64)
    if response[1] != 0:
        raise "failed to update bootup flash"

# writeBootupFlash()
# readChipSettings()

setDischarge(0)
setCharge(0)
setChargeCurrent(0)

parser = argparse.ArgumentParser(prog='BatteryCharger', description='Controls the USB charger')
parser.add_argument('--charge', action='store_true', help='Charge to 1.4V')
parser.add_argument('--charge-trickle', action='store_true', help='Trickle-charge to 1.4V')
parser.add_argument('--max-mAh', action='store', help='Stop charging once reaching given mAh')
parser.add_argument('--discharge', action='store_true', help='Discharge to 0.8V and estimate mAh')
parser.add_argument('--discharge-full', action='store_true', help='Discharge to 0.8V and keep there, estimate mAh')
parser.add_argument('--monitor', action='store_true', help='Monitor cell voltage')
parser.add_argument('--resume-time', action='store', help='Resume with start time as specified')
parser.add_argument('--resume-mAh', action='store', help='Resume with mAh as specified')
parser.add_argument('--fixed-current', action='store', help='Output specified current')
parser.add_argument('--full-cycle', action='store_true', help='Enlivens, fully discharges, re-charges, measures, re-charges')
args = parser.parse_args()

# 0 -> 12mA
# 1 -> 37mA
# 2 -> 60mA
# 3 -> 82mA
# 4 -> 102mA
# 5 -> 122mA
# 6 -> 139mA
# 7 -> 155mA (likely max on 5V)
CHARGE_CURRENTS = [0.012, 0.037, 0.060, 0.082, 0.102, 0.122, 0.139, 0.155]

# Observed discharge
# 1.26 * 25.5mA
# -> U = I * R
# -> R = U / I
# -> R = 1.26V / 0.0255A -> 49.41 Ohm => ~50 Ohm

anything = False
start = time.monotonic()
if args.resume_time:
    start = time.monotonic() - float(args.resume_time)

def initialAmpereSeconds():
    if args.resume_mAh:
        return float(args.resume_mAh) * 3600.0 / 1000.0
    return 0

if args.discharge:
    anything = True

    ampereSeconds = initialAmpereSeconds()
    step_start = start

    setDischarge(1)

    while True:
        time.sleep(0.1)
        voltage = readADC()

        step_end = time.monotonic()
        dt = step_end - step_start
        current = voltage / 50.0
        ampereSeconds += current * dt
        step_start = step_end

        print(json.dumps({
            "voltage": voltage,
            "mAh": ampereSeconds / 3600.0 * 1000.0,
            "t": time.monotonic() - start,
        }), flush=True)

        if voltage < 0.8:
            break

    setDischarge(0)

    print(json.dumps({
        "msg": "Discharge finished.",
    }), flush=True)

def dischargeFull():
    ampereSeconds = initialAmpereSeconds()
    step_start = start
    approxCurrent = 0

    setDischarge(1)

    while True:
        time.sleep(0.1)
        voltage = readADC()
        current = voltage / 50.0

        if voltage < 0.8:
            setDischarge(0)
            current = 0
        if voltage > 0.8:
            setDischarge(1)

        if approxCurrent == 0:
            approxCurrent = current
        else:
            approxCurrent = 0.99 * approxCurrent + 0.01 * current

        step_end = time.monotonic()
        dt = step_end - step_start
        ampereSeconds += current * dt
        step_start = step_end

        print(json.dumps({
            "voltage": voltage,
            "mAh": ampereSeconds / 3600.0 * 1000.0,
            "approxCurrent": approxCurrent,
            "discharge": discharge,
            "t": time.monotonic() - start,
        }), flush=True)

        if approxCurrent != 0 and approxCurrent < 0.005:
            break

    setDischarge(0)

    print(json.dumps({
        "msg": "Discharge finished.",
    }), flush=True)


if args.discharge_full:
    anything = True

    dischargeFull()

def runCharge(currentSelection):
    global anything

    anything = True

    ampereSeconds = initialAmpereSeconds()

    while True:
        setCharge(0)
        time.sleep(5)
        voltage = readADC()

        if voltage >= 1.4:
            break
        if args.max_mAh and ampereSeconds >= float(args.max_mAh) / 1000.0 * 3600.0:
            break

        print(json.dumps({
            "voltage": voltage,
            "mAh": ampereSeconds / 3600.0 * 1000.0,
            "t": time.monotonic() - start,
            "relaxed": 1,
        }), flush=True)

        desiredChargeCurrent = currentSelection(voltage)
        setChargeCurrent(desiredChargeCurrent)
        realChargeCurrent = CHARGE_CURRENTS[dac_bits]

        step_start = time.monotonic()
        setCharge(1)

        chargeCycleStart = time.monotonic()

        while time.monotonic() < chargeCycleStart + 60.0:
            time.sleep(0.1)
            voltage = readADC()

            step_end = time.monotonic()
            dt = step_end - step_start
            ampereSeconds += realChargeCurrent * dt
            step_start = step_end

            print(json.dumps({
                "voltage": voltage,
                "mAh": ampereSeconds / 3600.0 * 1000.0,
                "t": time.monotonic() - start,
                "chargeCurrent": realChargeCurrent,
            }), flush=True)

            if voltage >= 1.75:
                break

        setCharge(0)

    setCharge(0)

    print(json.dumps({
        "msg": "Charge finished.",
    }), flush=True)

def trickleCharge(voltage):
    return 15

def defaultCharge(voltage):
    if voltage < 1.375:
        return 160
    if voltage < 1.39:
        return 120
    if voltage < 1.4:
        return 80
    return 15

def fullCycle(currentSelection):
    global anything
    global start

    anything = True

    setChargeCurrent(150)
    setCharge(1)
    realChargeCurrent = CHARGE_CURRENTS[dac_bits]

    broken = False

    # Hard-Charge for 1s
    for i in range(0, 100):
        time.sleep(0.1)
        voltage = readADC()

        print(json.dumps({
            "voltage": voltage,
            "t": time.monotonic() - start,
            "chargeCurrent": realChargeCurrent,
            "msg": "Enliving...",
        }), flush=True)

    # Full discharge
    setCharge(0)
    dischargeFull()

    # Charge
    print(json.dumps({
        "msg": "Charging (for measurement).",
    }), flush=True)
    start = time.monotonic()
    runCharge(currentSelection)

    # Normal discharge
    print(json.dumps({
        "msg": "Discharging (measurement).",
    }), flush=True)
    start = time.monotonic()
    step_start = time.monotonic()
    ampereSeconds = 0

    setDischarge(1)
    while True:
        time.sleep(0.1)
        voltage = readADC()

        step_end = time.monotonic()
        dt = step_end - step_start
        current = voltage / 50.0
        ampereSeconds += current * dt
        step_start = step_end

        print(json.dumps({
            "voltage": voltage,
            "mAh": ampereSeconds / 3600.0 * 1000.0,
            "t": time.monotonic() - start,
        }), flush=True)

        if voltage < 0.8:
            break

    setDischarge(0)

    estimatedCapacity = ampereSeconds

    print(json.dumps({
        "msg": "Discharge finished, estimated mAh: " + str(int(estimatedCapacity * 1000 / 3600)),
    }), flush=True)

    start = time.monotonic()
    dischargeFull()

    print(json.dumps({
        "msg": "Charging.",
    }), flush=True)
    start = time.monotonic()
    runCharge(currentSelection)

    print(json.dumps({
        "msg": "Charging finished, estimated mAh: " + str(int(estimatedCapacity * 1000 / 3600)),
    }), flush=True)

if args.charge_trickle:
    runCharge(trickleCharge)

if args.charge:
    runCharge(defaultCharge)

if args.fixed_current:
    step_start = time.monotonic()

    setChargeCurrent(float(args.fixed_current))
    setCharge(1)

    ampereSeconds = initialAmpereSeconds()
    realChargeCurrent = CHARGE_CURRENTS[dac_bits]

    while True:
        time.sleep(0.1)
        voltage = readADC()

        step_end = time.monotonic()
        dt = step_end - step_start
        ampereSeconds += realChargeCurrent * dt
        step_start = step_end

        print(json.dumps({
            "voltage": voltage,
            "mAh": ampereSeconds / 3600.0 * 1000.0,
            "t": time.monotonic() - start,
            "chargeCurrent": realChargeCurrent,
        }), flush=True)

if args.full_cycle:
    fullCycle(defaultCharge)

if args.monitor:
    while True:
        time.sleep(0.1)
        voltage = readADC()

        print(json.dumps({
            "voltage": voltage,
            "t": time.monotonic() - start,
        }), flush=True)

if not anything:
    parser.print_help()
