#!/bin/sh

if [ "$1" = "" ]; then
  echo "Usage ./plot.sh <discharge.log>"
  exit 1;
fi

lines="$(grep voltage "$1" | wc -l | cut -d' ' -f 1)"
stepsize="$((lines / 10000 + 1))"

if [ "$DISPLAY" = "" ]; then
  grep voltage "$1" | \
    sed -ne "0~${stepsize}p" | \
    jq -r '(.t | tostring) + " " + (.voltage | tostring) + " " + (.approxCurrent | tostring)' | \
    gnuplot -e "set style line 1 lt 1 lw 1 pt 1 ps 0.5" \
            -e "set term xterm" \
            -e "plot '-' using 1:2 with lines ls 1 notitle"
else
  grep voltage "$1" | \
    sed -ne "0~${stepsize}p" | \
    jq -r '(.t | tostring) + " " + (.voltage | tostring) + " " + (.approxCurrent | tostring)' | \
    gnuplot --persist -e "set style line 1 lt 1 lw 1 pt 1 ps 0.5" \
                      -e "plot '-' using 1:2 with lines ls 1 notitle"
fi
